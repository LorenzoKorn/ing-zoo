package com.ing.zoo;

import com.ing.zoo.interfaces.LeafEater;
import com.ing.zoo.interfaces.MeatEater;
import com.ing.zoo.interfaces.Trickable;
import com.ing.zoo.models.animals.*;

import java.util.Scanner;

public class Zoo {
    private static String[] commands;
    private static Animal[] animals;
    private static Scanner scanner;

    public static void main(String[] args) {
        commands = new String[5];
        commands[0] = "hello";
        commands[1] = "give leaves";
        commands[2] = "give meat";
        commands[3] = "perform trick";
        commands[4] = "exit";

        animals = new Animal[]{
                new Lion("henk"),
                new Hippo("elsa"),
                new Pig("dora"),
                new Tiger("wally"),
                new Zebra("marty"),
                new Monkey("winston"),
                new BushViper("jack")
        };

        start();
    }

    public static void start() {
        scanner = new Scanner(System.in);
        String input;

        // keep asking for actions until user says 'exit'
        while (!(input = askAction()).equals(commands[4])) {
            if (input.equals(commands[0])) {
                sayHello();
            } else if (input.contains(commands[0])) {
                sayHello(input.split(" ")[1]);
            } else if (input.equals(commands[1])) {
                giveLeaves();
            } else if (input.equals(commands[2])) {
                giveMeat();
            } else if (input.equals(commands[3])) {
                showTrick();
            } else {
                System.out.println("Unknown command: " + input);
            }
        }
    }

    /**
     * Ask user for an action
     * @return action
     */
    private static String askAction() {
        System.out.print("Voer uw command in: ");
        return scanner.nextLine();
    }

    /**
     * All animals will greet you
     */
    private static void sayHello() {
        for (Animal animal : animals) {
            animal.sayHello();
        }
    }

    /**
     * All animals with the given name will greet you
     *
     * @param name String: name of the animals to greet you
     */
    private static void sayHello(String name) {
        for (Animal animal : animals) {
            if (animal.getName().equals(name)) {
                animal.sayHello();
            }
        }
    }

    /**
     * Feed animals that eat leaves some fresh leaves
     */
    private static void giveLeaves() {
        for (Animal animal : animals) {
            if (animal instanceof LeafEater) {
                ((LeafEater) animal).eatLeaves();
            }
        }
    }

    /**
     * Feed animals that eat meat some delicious meat
     */
    private static void giveMeat() {
        for (Animal animal : animals) {
            if (animal instanceof MeatEater) {
                ((MeatEater) animal).eatMeat();
            }
        }
    }

    /**
     * Let the animals show their skills if the have one
     */
    private static void showTrick() {
        for (Animal animal : animals) {
            if (animal instanceof Trickable) {
                ((Trickable) animal).performTrick();
            }
        }
    }
}
