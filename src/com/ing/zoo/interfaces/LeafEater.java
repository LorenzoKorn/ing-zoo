package com.ing.zoo.interfaces;

public interface LeafEater {
    void eatLeaves();
}
