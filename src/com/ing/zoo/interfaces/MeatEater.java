package com.ing.zoo.interfaces;

public interface MeatEater {
    void eatMeat();
}
