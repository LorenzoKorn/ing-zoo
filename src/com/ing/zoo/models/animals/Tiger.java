package com.ing.zoo.models.animals;

import com.ing.zoo.interfaces.Trickable;

import java.util.Random;

public class Tiger extends Carnivore implements Trickable {
    public String helloText;
    public String eatText;

    public Tiger(String name) {
        super(name);
    }

    public void sayHello() {
        helloText = "rraaarww";
        System.out.println(helloText);
    }

    public void eatMeat() {
        eatText = "nomnomnom oink wubalubadubdub";
        System.out.println(eatText);
    }

    public void performTrick() {
        String[] tricks = new String[]{
                "jumps in tree",
                "scratches ears"
        };
        Random random = new Random();
        int rnd = random.nextInt(tricks.length);
        System.out.println(tricks[rnd]);
    }
}
