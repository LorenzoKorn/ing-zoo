package com.ing.zoo.models.animals;

public class BushViper extends Carnivore{
    public String helloText;
    public String eatText;

    public BushViper(String name) {
        super(name);
    }

    public void sayHello() {
        helloText = "Sssuch a pleasssure to meet you, how issss you day?";
        System.out.println(helloText);
    }

    public void eatMeat() {
        eatText = "sjlp sjlp sjlp hiss hiss";
        System.out.println(eatText);
    }
}
