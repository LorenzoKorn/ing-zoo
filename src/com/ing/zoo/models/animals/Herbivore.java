package com.ing.zoo.models.animals;

import com.ing.zoo.interfaces.LeafEater;

public class Herbivore extends Animal implements LeafEater {

    public Herbivore(String name) {
        super(name);
    }

    public void eatLeaves() {
        System.out.println("Nom nom");
    }
}
