package com.ing.zoo.models.animals;

public class Animal {
    protected String name;

    public Animal(String name) {
        this.name = name;
    }

    public void sayHello() {
        System.out.println("Hello there");
    }

    public String getName() {
        return name;
    }
}
