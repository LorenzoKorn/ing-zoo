package com.ing.zoo.models.animals;

import com.ing.zoo.interfaces.Trickable;

import java.util.Random;

public class Zebra extends Herbivore implements Trickable {
    public String helloText;
    public String eatText;

    public Zebra(String name) {
        super(name);
    }

    public void sayHello() {
        helloText = "zebra zebra";
        System.out.println(helloText);
    }

    public void eatLeaves() {
        eatText = "munch munch zank yee bra";
        System.out.println(eatText);
    }

    public void performTrick() {
        String[] tricks = new String[]{
                "Spit dance",
                "clown dance"
        };
        Random random = new Random();
        int rnd = random.nextInt(tricks.length);
        System.out.println(tricks[rnd]);
    }
}
